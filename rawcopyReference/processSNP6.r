


rawcopy.SNP6 <- function(name, celfile, outdir, cdf, annot, pannot, snpClusters, probeClusters) {
    intensities <- matrix(celfile$intensities, nrow=2572, ncol=2680)
    rm(celfile)
    probeNames <- names(cdf)
    probeIx <- grep('CN',as.character(probeNames))
    snpIx <- grep('SNP',as.character(probeNames))
    ### Read SNPS
    snps <- data.frame(Name=probeNames[snpIx], ix=1:length(snpIx))
    baf <- raf <- lrr <- lr <- rep(NA, nrow(snps))
    for (i in 1:length(a)) {
        ix1 <- cdf[[snpIx[i]]][[1]][[1]][[1]]
        ix2 <- cdf[[snpIx[i]]][[1]][[2]][[1]]
        ta <- (intensities[ix1])
        tb <- (intensities[ix2])
        outliers=outliers(ta,tb)
        ta <- mean(ta[!outliers])
        tb <- mean(tb[!outliers])
        lr[i] <- log2(sqrt(ta^2+tb^2))
        raf[i] <- tb/(ta+tb)
    }
    ### Read probes
    probes <- data.frame(Name=probeNames[probeIx], ix=1:length(probeIx))
    lir <- li <- rep(NA, nrow(probes))
    for (i in 1:length(rint)) {
        ix <- cdf[[probeIx[i]]][[1]][[1]][[1]]
        li[i] <- log2(mean(intensities[ix])) # nearly always just one.
    }
    ## RAW SNPs and probes are now stored as raf+lr and li.

    ## Compute BAF (SNPs) -> snps.txt
    for (i in which(probeClusters$good)) {
        
    }
    
    ## Compute raw Log-R-Ratio (SNPs)
    
    ## Normalize Log-R-Ratio (SNPs)
    
    ## Compute raw Log-Intensity-Ratio (probes)
    
    ## Normalize Log-Intensity-Ratio (probes)
    
    ## Merge to "Log-R" -> probes.txt
    
    ## Segmentation
    
    ## Compute data for segments -> segments.txt
    
    ## Save data files
    
    
    ## Last, produce QC and figure.
    
    
    
    
}