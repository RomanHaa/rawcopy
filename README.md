# Rawcopy #

Rawcopy is an R package for processing of **Affymetrix CytoScan HD**, **CytoScan 750k** and **SNP 6.0** microarrays for copy number analysis. It takes CEL files (raw microarray intensity values) as input. Output consists of:

**Log ratio:** normalized intensity per probe relative to sample median and a reference data set

**B-allele frequency or BAF:** estimated abundance of the B allele relative to total abundance, SNP probes only

**Segments:** genomic segments of unchanging copy number, estimated using the PSCBS package

**Figures:** several figures per sample and sample set are plotted for the user's convenience, some examples are shown below