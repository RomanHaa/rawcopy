pkgname <- "rawcopy"
source(file.path(R.home("share"), "R", "examples-header.R"))
options(warn = 1)
library('rawcopy')

base::assign(".oldSearch", base::search(), pos = 'CheckExEnv')
cleanEx()
nameEx("rawcopy.rawcopy")
### * rawcopy.rawcopy

flush(stderr()); flush(stdout())

### Name: rawcopy
### Title: The wrapper for CEL file processing
### Aliases: rawcopy

### ** Examples

rawcopy('~')



### * <FOOTER>
###
options(digits = 7L)
base::cat("Time elapsed: ", proc.time() - base::get("ptime", pos = 'CheckExEnv'),"\n")
grDevices::dev.off()
###
### Local variables: ***
### mode: outline-minor ***
### outline-regexp: "\\(> \\)?### [*]+" ***
### End: ***
quit('no')
