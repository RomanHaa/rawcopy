



#     Copyright (C) 2015  Markus Mayrhofer, Björn Viklund
# 
#     This source code is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; version 2.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#     
#     Some data contained in this R-package are derived from files
#     provided by Affymetrix (.cdf, .cn.annot.csv and .annot.csv),
#     and which are copyrighted by Affymetrix. Redistribution and
#     modification of these data may be prohibited by Affymetrix.
#     





segment.wrapper <- function(names,dirs,plot=FALSE,method='CBS',alpha=0.005) {
    
    ## load correct data and merge for segmentation
    probes=NULL
    snps=NULL
    for (i in 1:length(names)) {
        #browser()
        load(paste(dirs[i],'rawcopy.Rdata',sep='/'))
        if (is.null(probes)) probes=probes.txt[,1:4]
        probes[[names[i]]]=probes.txt[,5]
        if (is.null(snps)) { 
            ix=snps.txt$Quality==1
            snps=snps.txt[ix,1:4] 
        }
        snps[[names[i]]]=snps.txt[ix,5]
    }
    #browser()
    ## remove CN markers that "contain" a SNP marker
    ix=which(diff(probes$Start)>0 & diff(probes$End)<0)
    probes=probes[-ix,]
    gc=gc[-ix]
    
    ## do the segmentation
    segments.txt <- rawcopy.segment(names,probes,snps,gc,show=plot,method=method)
    #snpsegments.txt <- rawcopy.snpsegment(names,snps,show=plot) #used only for germline LOH
    
    ## for each sample included: Store segments as segment table and gene copy numbers as gene table
    for (i in 1:length(names)) {
        t <- segments.txt[,1:3]
        t$Value <- segments.txt[[names[i]]]
        t$Allelic.Imbalance <- segments.txt[[paste('Allelic.Imbalance.',names[i],sep='')]]
        save.txt(t,file=paste(dirs[i],'segments.txt',sep='/'))
        save.txt(segments.txt,file=paste(dirs[i],'segments2.txt',sep='/'))
        #browser()
        ### Compute log ratio per gene from segments
        data('refseq.genes',package='rawcopy')
        for (j in 1:nrow(t)) {
            ix <- refseq.genelist$chrom==as.character(t$Chromosome[j]) & 
                (refseq.genelist$txStart+refseq.genelist$txEnd)/2>t$Start[j] &
                (refseq.genelist$txStart+refseq.genelist$txEnd)/2<t$End[j]
            refseq.genelist$Value[ix] <- t$Value[j]
            refseq.genelist$Allelic.Imbalance[ix] <- t$Allelic.Imbalance[j]
        }
        save(refseq.genelist,file=paste(dirs[i],'genelist.Rdata',sep='/'))
    }

}


## This function performs segmentation of ONE OR MULTIPLE MATCHED samples.
rawcopy.segment <- function(names,probes,snps=NULL,gc=NA,windows=NULL,show=FALSE,method='CBS',alpha=0.005) {
    plot=show
    min.width=5
    separate=1e6
    data('chromData',package='rawcopy')
    data('chromLengths',package='rawcopy')
    data('refseq.genes',package='rawcopy')
    
                            
    probes.txt=probes
    snps.txt=snps

    #browser()
    chroms <- as.character(unique(probes.txt$Chromosome)); chroms=chroms[!is.na(chroms)]; chroms=chroms[order(chrom_n(chroms))]
    segments.txt <- NULL
    # probes.txt and snps.txt are expected to be pre-merged so that there are multiple data cols (order same as "names")
    nSamples=length(names)
    logr.cols=(which(colnames(probes.txt)=='End')+1):(which(colnames(probes.txt)=='End')+nSamples)
    if (!is.null(snps)) { 
        baf.cols=(which(colnames(snps.txt)=='End')+1):(which(colnames(snps.txt)=='End')+nSamples)
        if (length(logr.cols)!=length(baf.cols)) cat('DATA COLUMNS IN "PROBES" and "SNPS" DO NOT MATCH\n')
    }
    ## Test-segment 1p for setting of stringency
    
#     browser()
#     ix=probes.txt$Chromosome=='chr1' & probes.txt$Start<40e6; ix[is.na(ix)]=F
#     logr=data.frame(probes.txt[ix,logr.cols])
#     lpos=round((probes.txt$Start[ix]+probes.txt$End[ix])/2)
#     t_gc=NA; if (length(gc)>1) t_gc=gc[ix]
#     temp_baf <- temp_bpos <- NULL
#     for (alpha in c(1e-2,1e-3,1e-6,1e-8,1e-10)) {
#         seg=getCBSsegments(logr, lpos, temp_baf, temp_bpos,t_gc[ix], windows,plot,alpha=alpha,min.width=min.width,undo.splits='sdundo')
#         if (nrow(seg)<15) break
#     }
    for (c in chroms) { #chroms
        #cat(c)
        #browser()
        ix=probes.txt$Chromosome==c; ix[is.na(ix)]=F
        logr=data.frame(probes.txt[ix,logr.cols])
        lpos=round((probes.txt$Start[ix]+probes.txt$End[ix])/2)
        t_gc=NA; if (length(gc)>1) t_gc=gc[ix]
        baf <- bpos <- NULL
        if (!is.null(snps)) {
            ix=snps.txt$Chromosome==c; ix[is.na(ix)]=F
            baf=data.frame(snps.txt[ix,baf.cols])
            bpos=(snps.txt$Start[ix]+snps.txt$End[ix])/2
        }
        # centromere cutoff=1Mb
        dists=diff(lpos)
        cuts=c(0,which(dists>separate),length(lpos))
        #if (c=='chr2') browser()
        for (i in 2:length(cuts)) {
            #cat(cuts, ':', i, '\n')
            ix=(cuts[i-1]+1):cuts[i]
            temp_baf <- temp_bpos <- NULL
            if (!is.null(snps)){ 
                bix=bpos>=lpos[ix][1] & bpos<=lpos[ix][length(ix)]
                if (sum(bix)>100) {
                    temp_baf=baf[bix,] 
                    temp_bpos=bpos[bix]
                }
            }
            segments.txt <- rbind(segments.txt,
                                  cbind(c,
                                        getCBSsegments(logr[ix,], 
                                                       lpos[ix], 
                                                       temp_baf, 
                                                       temp_bpos,t_gc[ix], 
                                                       plot,
                                                       alpha,
                                                       min.width=5,
                                                       method=method)))
        }
    }
    names(segments.txt)[1]='Chromosome'
    
    #browser()
    
    
    ## Compute data for segments -> segments.txt
    cat(format(Sys.time(), "%F %H:%M:%S"),'Segment analysis:',names,'\n')
    #save.image('temp.Rdata')
    #browser()
    segments.txt$Length.Mbp=round((segments.txt$End-segments.txt$Start)/1e6,2)
    for (i in 1:nSamples) segments.txt[[names[i]]]=NA
    segments.txt$Markers=NA
    for (i in 1:nSamples) segments.txt[[paste('Allelic.Imbalance.',names[i],sep='')]]=NA
    segments.txt$SNPs=NA
    segments.txt$Start.band=NA
    segments.txt$End.band=NA
    segments.txt$Refseq.genes=addGenes(segments.txt[,1:3],refseq.genelist)$genes 
    
    probe.indices=segMatch(segments.txt,probes.txt)
    snp.indices=segMatch(segments.txt,snps.txt)
    
    for (s in 1:nrow(segments.txt)) {
        #cat(s)
        if (!is.na(probe.indices[s,1])) {
            logr=data.frame(probes.txt[probe.indices[s,1]:probe.indices[s,2],logr.cols])
            for (i in 1:nSamples) segments.txt[[names[i]]][s]=round(median(logr[,i],na.rm=T),3)
            segments.txt$Markers[s]=nrow(logr)
        }
        if (!is.na(snp.indices[s,1])) {
            baf=data.frame(snps.txt[snp.indices[s,1]:snp.indices[s,2],baf.cols])
            segments.txt$SNPs[s]=nrow(baf)
            baf=data.frame(baf[!is.na(baf),])
            for (i in 1:nSamples) segments.txt[[paste('Allelic.Imbalance.',names[i],sep='')]][s]=round(getAI(baf[,i])[1],3)
        }
        c=as.character(segments.txt$Chromosome[s])
        segments.txt$Start.band[s]=paste(substr(c,4,6),as.character(chromData$name[chromData$chr==c &
                                                                                       chromData$chromStart<=segments.txt$Start[s] &
                                                                                       chromData$chromEnd>=segments.txt$Start[s]]),sep='')
        segments.txt$End.band[s]=paste(substr(c,4,6),as.character(chromData$name[chromData$chr==c &
                                                                                     chromData$chromStart<=segments.txt$End[s] &
                                                                                     chromData$chromEnd>=segments.txt$End[s]]),sep='')
    }
    return(segments.txt)
}

getCBSsegments <- function(logr, lpos, baf, bpos,gc=NA, plot=F,alpha,min.width=5,method='CBS') {
    

    if (is.vector(logr)) logr=data.frame(logr) # in case a single sample is submitted
    if (is.vector(baf) & !is.null(baf)) baf=data.frame(baf)
    ns=ncol(logr)
    
    contig.start=round(min(min(lpos),min(c(bpos,Inf)))-12)
    contig.end=round(max(max(lpos),max(c(bpos,-Inf)))+12)
    
    breakpos1  <- breakpos2 <- NULL
    
    logr.sum=apply(X=logr,MARGIN=1,FUN=sum)
    na=is.na(logr.sum)
    lpos=lpos[!na]
    logr=data.frame(logr[!na,])
    logr.sum=logr.sum[!na]
    if (length(gc)>1) gc=gc[!na]
    
    #breakpos1=c(0,nrow(logr))
    #### Using CBS while developing the above
    #logr.sum, logr, lpos, baf, bpos
    y=norm(logr.sum,gc,0.1)
    #mapd=median(abs(diff(y)),na.rm=T)
    #y=y*(0.5/mapd); y=2+y-median(y,na.rm=T)
    
    ## Here: If only one sample, use unmatched PSCBS instead:
    #browser()
    
    fit=NULL
    if (method=='PSCBS' & ns==1 & !is.null(baf[,1])) suppressWarnings(try( {
        ix=match(lpos,bpos)
        
        fit=segmentByNonPairedPSCBS(CT=2*2^y,betaT=baf[ix,1],preserveScale=F,alphaTCN=alpha,alphaDH=alpha,min.width=min.width)

        #plot(y)
        #segments(x0=fit$tcnSegRows$startRow[-1],x1=fit$tcnSegRows$startRow[-1],y0=0,y1=1,col='green')
        #plot(lpos,baf[ix,1])
        #segments(x0=lpos[fit$dhSegRows$startRow[-1]],x1=lpos[fit$dhSegRows$startRow[-1]],y0=0,y1=1,col='green')
        breakpos1=c(0,ceiling(fit$output$tcnStart[-1])-1,length(y))
    },silent=T))
    
    if (is.null(breakpos1)) {
        #cat('Hey!')
        data <- data.frame(x=lpos,y=y)
        fit <- segmentByCBS(data,verbose=0,min.width=min.width,alpha=alpha,undo.splits='sdundo',undo.SD=0.2)
        breakpos1=c(0,fit$segRows$startRow[-1]-1,length(y))
        dcut=0.05; d <- rep(0,length(breakpos1)); p <- rep(1,length(breakpos1))
        if (length(breakpos1)>2) for (i in (length(breakpos1)-1):2) {
            left=breakpos1[i-1]
            right=breakpos1[i+1]
            for (s in 1:ns) {
                #browser()
                dif=abs(median(logr[(left+1):breakpos1[i],s])-median(logr[(breakpos1[i]+1):(right),s]))
                #suppressWarnings(
                #   pval <- t.test(logr[(left+1):breakpos1[i],s],logr[(breakpos1[i]+1):(right),s])$p.value)
                #cat(dif,'\n')
                d[i]=max(d[i],dif)
                #p[i]=min(p[i],pval)
            }
            if (d[i]<dcut) breakpos1=breakpos1[-i]
        }
    }
    

    
    if (plot) {
        try(dev.off(),silent=T)
        #plot(logr.sum,ylim=c(-3,3),pch=16,cex=0.2,col='#00000050')
        #segments(x0=breakpos1,x1=breakpos1,y0=-3,y1=3,col='#00FF00')
        plot(logr.sum,ylim=c(-3,3),pch=16,cex=0.2,col='#00000050')
        try(segments(x0=breakpos1,x1=breakpos1,y0=-3,y1=3,col='#00FF00'),silent=T)
    }

    ### convert breakpoints to positions
    logbreaks=(lpos[trim(breakpos1)]+lpos[trim(breakpos1)+1])/2
    
    ## get breakpoints from BAF
    ## (Separately per sample)
    if (F & !is.null(baf)) {
        baf.all=baf
        bpos.all=bpos
        breakpos=numeric(0)
        
        tseg <- data.frame(Chromosome='X',Start=logbreaks[-length(logbreaks)],End=logbreaks[-1])
        tpos <- data.frame(Chromosome='X',Start=bpos.all,End=bpos.all)
        tmatch <- segMatch(tseg,tpos) # now contains start and end indices for any SNPs in each "logbreaks" segment
        
        newBreaks <- NULL # SNP indexes that begin a new segment
        
        for (i in 1:nrow(tmatch)) if (!is.na(tmatch$startIx[i])) if (sum(tmatch[i,])>300) { 
            # for each primary CBS segment, will perform segmentation on summarized allelic imbalance if longer than 300 SNPs
            
            # Table of 30-SNP subsegments for which allelic imbalance can be calculated
            ai.table=data.frame(startIx=seq(tmatch$startIx[i],tmatch$endIx[i],30),
                                endIx=seq(tmatch$startIx[i],tmatch$endIx[i],30)+29,
                                ai=0)
            
            for (j in 1:nrow(ai.table)) for (s in 1:ns) { 
                # AI calculated separately per subsegment and per sample in case of matched segmentation
                baf=baf.all[ai.table$startIx[j]:ai.table$endIx[j],s] # baf is now vector of current sample and current primary segment
                ai=getAI(baf)
                ai.table$ai[j]=ai.table$ai[j]+ai[1]/ai[2]
            }
            
            data=data.frame(x=ai.table$start,y=((ai.table$ai)))
            #data$y[30:48]=data$y[30:48]+0.25
            segRows <- segmentByCBS(data,verbose=0,min.width=5,alpha=0.03)$segRows
            
            if (nrow(segRows)>1) newBreaks <- c(newBreaks,ai.table$start[segRows$startRow[-1]])
            
            ## not forget to add newBreaks
        }
    } #end baf section
    breakpos=c(contig.start,round(sort(logbreaks)),contig.end)
    ## return segments
    Start=trim(breakpos,0,1)
    End=trim(breakpos,1,0)
    return(data.frame(Start,End))
}

getCBSsegments_old <- function(logr, lpos, baf, bpos,gc=NA, plot=F,alpha=0.01,min.width=5) {
    #browser()
    
    if (is.vector(logr)) logr=data.frame(logr) # in case a single sample is submitted
    if (is.vector(baf)) baf=data.frame(baf)
    ns=ncol(logr)
    
    contig.start=round(min(min(lpos),min(c(bpos,Inf)))-12)
    contig.end=round(max(max(lpos),max(c(bpos,-Inf)))+12)
    
    breakpos1  <- breakpos2 <- NULL
    
    logr.sum=apply(X=logr,MARGIN=1,FUN=sum)
    na=is.na(logr.sum)
    lpos=lpos[!na]
    logr=data.frame(logr[!na,])
    logr.sum=logr.sum[!na]
    if (length(gc)>1) gc=gc[!na]
    
    #breakpos1=c(0,nrow(logr))
    #### Using CBS while developing the above
    #logr.sum, logr, lpos, baf, bpos
    y=norm(logr.sum,gc,0.1)
    #mapd=median(abs(diff(y)),na.rm=T)
    #y=y*(0.5/mapd); y=2+y-median(y,na.rm=T)
    data <- data.frame(x=lpos,y=y)
    fit <- segmentByCBS(data,verbose=0,min.width=min.width,alpha=alpha)#,undo.splits='sdundo',undo.SD=3)
    breakpos1=c(0,fit$segRows$startRow[-1]-1,length(y))
    #short=which(diff(breakpos1)<minSeg)
    #use=round((breakpos1[short]+breakpos1[short+1])/2)
    #breakpos1=sort(c(use,breakpos1[-c(short,short+1)]))
    #     dcut=0.04; d <- rep(0,length(breakpos1)); p <- rep(1,length(breakpos1))
    #     if (length(breakpos1)>2) for (i in (length(breakpos1)-1):2) {
    #         left=breakpos1[i-1]
    #         right=breakpos1[i+1]
    #         for (s in 1:ns) {
    #             #browser()
    #             dif=abs(median(logr[(left+1):breakpos1[i],s])-median(logr[(breakpos1[i]+1):(right),s]))
    #             suppressWarnings(
    #                 pval <- t.test(logr[(left+1):breakpos1[i],s],logr[(breakpos1[i]+1):(right),s])$p.value)
    #             #cat(dif,'\n')
    #             d[i]=max(d[i],dif)
    #             p[i]=min(p[i],pval)
    #         }
    #         if (p[i]>pcut | d[i]<dcut) breakpos1=breakpos1[-i] #replace with removal of worst in order
    #     }
    
    if (plot) {
        try(dev.off(),silent=T)
        #plot(logr.sum,ylim=c(-3,3),pch=16,cex=0.2,col='#00000050')
        #segments(x0=breakpos1,x1=breakpos1,y0=-3,y1=3,col='#00FF00')
        plot(logr.sum,ylim=c(-3,3),pch=16,cex=0.2,col='#00000050')
        try(segments(x0=breakpos1,x1=breakpos1,y0=-3,y1=3,col='#00FF00'),silent=T)
    }
    #browser()
    
    ### convert breakpoints to positions
    logbreaks=(lpos[trim(breakpos1)]+lpos[trim(breakpos1)+1])/2
    
    ## get breakpoints from BAF
    ## Do it separately per sample
    if (!is.null(baf)) {
        baf.all=baf
        bpos.all=bpos
        breakpos=numeric(0)
        for (s in 1:ns) {
            #browser()
            baf=baf.all[,s] # baf is now vector of current sample
            bpos=bpos.all
            baf=abs(baf-.5) 
            #baf[baf>0.45]=0.45 ## may need to be adjusted
            bpos=bpos[!is.na(baf)]
            baf=baf[!is.na(baf)]
            ranks=1:length(bpos)
            ranks=ranks[baf<0.4]
            diffs=c(median(diff(ranks)),diff(ranks))
            y=diffs
            data=data.frame(x=bpos[baf<0.4],y)
            fit <- segmentByCBS(data, verbose = 0)
            breakpos2=c(0,ranks[fit$segRows$startRow[-1]-1],length(baf))
            
            #if (plot) plot(baf)
            #logr.sum, logr, lpos, baf, bpos
            
            #y=quant_norm(baf)
            #data=data.frame(x=bpos,y)
            #fit <- segmentByCBS(data, verbose = 0)
            #breakpos2=c(0,fit$segRows$startRow[-1]-1,length(baf))
            
            #if (plot) segments(x0=breakpos2,x1=breakpos2,y0=-1,y1=10000,col='red')
            #short=which(diff(breakpos2)<minLOH)
            #use=round((breakpos2[short]+breakpos2[short+1])/2)
            #breakpos2=sort(c(use,breakpos2[-c(short,short+1)]))
            #         dcut=0.1; pcut=1e-5; d <- rep(0,length(breakpos2)); p <- rep(1,length(breakpos2))
            #         if (length(breakpos2)>2) for (i in (length(breakpos2)-1):2) {
            #             left=breakpos2[i-1]
            #             right=breakpos2[i+1]
            #             for (s in 1:ns) {
            #                 #browser()
            #                 dif=abs(median(logr[(left+1):breakpos2[i],s])-median(logr[(breakpos2[i]+1):(right),s]))
            #                 suppressWarnings(
            #                     pval <- wilcox.test(logr[(left+1):breakpos2[i],s],logr[(breakpos2[i]+1):(right),s])$p.value)
            #                 #cat(dif,'\n')
            #                 d[i]=max(d[i],dif)
            #                 p[i]=min(p[i],pval)
            #             }
            #             if (p[i]>pcut | d[i]<dcut) breakpos2=breakpos2[-i] #replace with removal of worst in order
            #         }
            #positions
            bafbreaks=(bpos[trim(breakpos2)]+bpos[trim(breakpos2)+1])/2
            # prune BAF breakpoints, no nearer than 300k to already-made breaks
            if (length(bafbreaks)>0 & length(logbreaks)>0) for (i in length(bafbreaks):1) {
                d=min(abs(bafbreaks[i]-logbreaks))
                if (d<.3e5) bafbreaks = bafbreaks[-i]
            }
            ## BAFbreaks were made for the current sample, add them to the "done" breaks
            logbreaks=c(round(sort(c(logbreaks,bafbreaks))))
            
            #if (plot) try(segments(x0=bafbreaks,x1=bafbreaks,y0=-3,y1=3,col='#0000FF'),silent=T)
        }
    } #end baf section
    breakpos=c(contig.start,round(sort(logbreaks)),contig.end)
    ## return segments
    Start=trim(breakpos,0,1)
    End=trim(breakpos,1,0)
    browser()
    return(data.frame(Start,End))
}

mgetSegments <- function(logr, lpos, baf, bpos,gc=NA, windows) {
    
    if (is.vector(logr)) logr=data.frame(logr) # in case a single sample is submitted
    if (is.vector(baf)) baf=data.frame(baf)
    ns=ncol(logr)
    
    contig.start=round(min(min(lpos),min(c(bpos,Inf)))-12.5)
    contig.end=round(max(max(lpos),max(c(bpos,-Inf)))+12.5)
    
    breakpos1  <- breakpos2 <- NULL
    
    logr.sum=apply(X=logr,MARGIN=1,FUN=sum)
    na=is.na(logr.sum)
    lpos=lpos[!na]
    logr=data.frame(logr[!na,])
    logr.sum=logr.sum[!na]
    if (length(gc)>1) gc=gc[!na]
    
    breakpos1=c(0,nrow(logr))
    
    # first split using short windows, then use longer windows on remains
    mainwindows=windows$mainwindows 
    oppositewindows=windows$oppositewindows
    cutdifs=windows$cutdifs
    cutpvals=windows$cutpvals
    
    #browser()
    for (i in 1:length(mainwindows)) { # For each set of main and opposite windows
        cat('\n',mainwindows[i],oppositewindows[i],'\n')
        for (j in 2:length(breakpos1)) if (breakpos1[j]-breakpos1[j-1] > mainwindows[i]*2) { # And for each long-enough already separated subsegment
            cat(j,': ')
            ix=(breakpos1[j-1]+1):breakpos1[j] # the current logr indices
            #browser()
            mapd=median(abs(diff(logr.sum[ix])),na.rm=T) # will adjust local cutoffs based on this MAPD value
            cat(' MAPD: ',round(mapd,2))
            d=cutdifs[i]*(mapd/0.2) # adjusted cutdif is based on size of MAPD relative to the 'norm' 0.20
            breaks=getBreaks(logr.sum[ix],mainwindows[i],oppositewindows[i],difcut=d,plot=F,gc=gc[ix]) # get potential breaks using sum of logr
            cat(', break ratio:', round(length(breaks)/length(ix),3),'\n')
            # if multiple samples, add individual breaks from them here??
            ## Test if breaks are good ("undo"). Here, each sample is tested separately.
            if (length(breaks)>0) {
                p <- rep(1,length(breaks))
                d <- rep(0,length(breaks))
                for (k in length(breaks):1) {
                    if (k==1) left=0 else left=breaks[k-1]
                    if (k==length(breaks)) right=length(logr.sum) else right=breaks[k+1]
                    #browser()
                    for (s in 1:ns) {
                        dif=abs(mean(logr[(left+1):breaks[k],s])-mean(logr[(breaks[k]+1):(right),s]))
                        d[k]=max(d[k],dif)
                        if (d[k]>cutdifs[i]) { 
                            #pval=t.test(logr[(left+1):breaks[k],s],logr[(breaks[k]+1):(right),s])$p.value[[1]]
                            #p[k]=min(p[k],pval)
                            p[k]=0
                        }
                    }
                    if (p[k]>cutpvals[i]) breaks=breaks[-k]
                }
                cat(length(breaks), ' breakpoints were kept.\n')
            }
            breakpos1=c(breakpos1,breaks+breakpos1[j-1]) # add these breakpoints into the complete set of cutting points, adjusting to relative position
        }
        breakpos1=sort(unique(breakpos1))
    }
    
    ## Test again if cuts are acceptable ("undo")
    if (F) {
        dcut=0.03
        for (pcut in c(1e-2,1e-3,1e-4,1e-5)) if (length(breakpos1)>2) {
            p <- rep(1,length(breakpos1))
            d <- rep(0,length(breakpos1))
            if (length(breakpos1)>2) for (i in (length(breakpos1)-1):2) {
                left=breakpos1[i-1]
                right=breakpos1[i+1]
                for (s in 1:ns) {
                    dif=abs(mean(logr[(left+1):breakpos1[i],s])-mean(logr[(breakpos1[i]+1):(right),s]))
                    d[i]=max(d[i],dif)
                    if (d[i]>dcut) {
                        #pval=t.test(logr[(left+1):breakpos1[i],s],logr[(breakpos1[i]+1):(right),s])$p.value[[1]]
                        #p[i]=min(p[i],pval)
                        p[i]=0
                    }
                }
                if (p[i]>pcut) breakpos1=breakpos1[-i]
            } 
        }
    }
 
    ### convert breakpoints to positions
    logbreaks=(lpos[trim(breakpos1)]+lpos[trim(breakpos1)+1])/2
    breakpos=c(contig.start,round(sort(logbreaks)),contig.end)
    
    try(dev.off())
    #plot(logr.sum,ylim=c(-3,3),pch=16,cex=0.2,col='#00000050')
    #segments(x0=breakpos1,x1=breakpos1,y0=-3,y1=3,col='#00FF00')
    plot(logr.sum,ylim=c(-3,3),pch=16,cex=0.2,col='#00000050')
    segments(x0=breakpos1,x1=breakpos1,y0=-3,y1=3,col='#00FF00')
    
    
    
    ## get breakpoints from BAF
    ## Do it separately per sample
    baf.all=baf
    breakpos=numeric(0)
    for (s in 1:ns) {
        baf=baf.all[,s] # baf is now vector of current sample
        baf=abs(baf-.5) 
        baf[baf>0.4]=NA ## may need to be adjusted
        bpos=bpos[!is.na(baf)]
        baf=baf[!is.na(baf)]
        
        if (length(baf)>300) {
            breakpos2=c(0,length(baf))
            
            # first split into longer segments, then find shorter ones in remaining stretches of data
            mainwindows=c(100) # testing a set of window lenghs
            oppositewindows=c(100)
            cutdifs=c(0.05)
            
            for (i in 1:length(mainwindows)) { # For each set of main and opposite windows
                #cat('\n',i,'\n')
                for (j in 2:length(breakpos2)) if (breakpos2[j]-breakpos2[j-1] > mainwindows[i]+oppositewindows[i]) { # And for each long-enough already separated subsegment
                    #cat(j)
                    ix=(breakpos2[j-1]+1):breakpos2[j] # the current baf indices
                    breaks1=getBreaks(baf[ix],mainwindows[i],oppositewindows[i],difcut=cutdifs[i],plot=F,gc=NA) # get potential breaks using mainwindow on left
                    #breaks2=getBreaks(baf[ix],oppositewindows[i],mainwindows[i],difcut=cutdifs[i],plot=F) # get potential breaks using mainwindow on right
                    breaks=breaks1#sort(unique(c(breaks1,breaks2))) # fuse breaks into a set of candidates
                    
                    ## Test if breaks are good ("undo")
                    if (length(breaks)>0) {
                        p <- d <- NULL; 
                        for (k in length(breaks):1) {
                            if (k==1) left=0 else left=breaks[k-1]
                            if (k==length(breaks)) right=length(baf) else right=breaks[k+1]
                            p[k]=t.test(baf[(left+1):breaks[k]],baf[(breaks[k]+1):(right)])$p.value[[1]]
                            #d[k]=abs(mean(baf[(left+1):breaks[k]])-mean(baf[(breaks[k]+1):(right)]))
                            if (p[k]>0.00001) breaks=breaks[-k]
                        } 
                    }
                    breakpos2=c(breakpos2,breaks+breakpos2[j-1]) # add these breakpoints into the complete set of cutting points, adjusting to relative position
                }
                breakpos2=sort(unique(breakpos2))
            }
            
            t=breakpos2
            ## Test again if cuts are good ("undo")
            for (pcut in c(1e-6)) if (length(breakpos2)>2) {
                p <- d <- rep(0,length(breakpos2));
                for (i in (length(breakpos2)-1):2) {
                    left=breakpos2[i-1]
                    right=breakpos2[i+1]
                    p[i]=t.test(baf[(left+1):breakpos2[i]],baf[(breakpos2[i]+1):(right)])$p.value[[1]]
                    #d[i]=abs(mean(baf[(left+1):breakpos2[i]])-mean(baf[(breakpos2[i]+1):(right)]))
                    if (p[i]>pcut) breakpos2=breakpos2[-i]
                } 
            }
            #browser()
            #plot(baf,pch=16,cex=0.2,col='#00000015')
            #segments(x0=breakpos2,x1=breakpos2,y0=-2,y1=2,col='#FF0000')

            ### convert breakpoints to positions
            bafbreaks=(bpos[trim(breakpos2)]+bpos[trim(breakpos2)+1])/2
            
            # prune BAF breakpoints, no nearer than 300k to already-made breaks
            if (length(bafbreaks)>0 & length(breakpos)>0) for (i in length(bafbreaks):1) {
                d=min(abs(bafbreaks[i]-breakpos))
                if (d<.3e5) bafbreaks = bafbreaks[-i]
            }
            breakpos=round(sort(c(bafbreaks,breakpos)))
        }
    }
    
    
    ## return segments
    Start=trim(breakpos,0,1)
    End=trim(breakpos,1,0)
    return(data.frame(Start,End))
}

getBreaks <- function(data,w1,w2,difcut,plot=F,gc=NA,margin=10) {
    #browser()
    if (length(data)<margin*2) return (NULL) # will not run unless this subregion is longer than 2x mainwindow
    na=is.na(data) # na sites removed temporarily
    data=data[!na]
    gc=gc[!na]
    
    if (length(gc)>1 & length(data)>100) data=norm(data=data,factor1=gc,window=0.1)
    dif1=getDiff(data,w1,w2,margin)                              
    dif2=getDiff(data,w2,w1,margin)
    dif=apply(X=data.frame(dif1,dif2),MARGIN=1,FUN=max)
    
    if (plot) plot(dif)
    peaks=getPeaks(dif,F) & dif>difcut
    # Keep only best peak in each window:
    #browser()
    for (i in which(peaks)[order(dif[peaks],decreasing=T)]) {
        left=max(1,i-w1)
        right=min(length(dif),i+w1)
        if (max(dif[left:right])!=dif[i]) peaks[i]=F
    }
    if (plot) points(x=(1:length(peaks))[peaks],
                     y=dif[peaks],
                     pch=2,col='red')
    out=rep(F,length(na)) # vector of length of orig. data
    out[!na][which(peaks)]=T
    return(which(out))
}

# Calculate dif at each site given w1 w2 and margin:
getDiff <- function(data,w1,w2,margin) {
    n=length(data)
    dif=rep(0,n-1) # length of dif is all possible breakpoints including those too close to edge
    i=min(margin,w1) # i is the current possible cutting site to be evaluated. Starts at margin pts or leftWindow(w1), whatever is smaller.
    n1=i # the current number of markers in each window
    n2=min(w2,n-margin)
    leftsum=sum(data[1:i]) # the current sum of values in each window
    rightsum=sum(data[(i+1):(min(i+w2,n))])
    dif[i]=abs(leftsum/n1-rightsum/n2) # the difference between these window means
    end=max(n-w2,n-margin) # Will end testing where w2 starts or margin values from subregion end, whatever is bigger
    for (i in (i+1):end) { # all possible breakpoints given w1 and w2
        #cat(i)
        #browser()
        if (i<w1) n1=i else n1=w1
        if (i>(n-w2)) n2=n-i else n2=w2
        if (i<=w1 & !(n-i)<w2) { # if currently nearer to left edge than w1
            leftsum=leftsum+data[i]
            rightsum=rightsum-data[i]+data[i+w2]
            dif[i]=abs(leftsum/n1-rightsum/n2)*(n1/w1)^.5 #a dampening function will punish the result near the edge.
        } else if (!i<=w1 & (n-i)<w2) { #if currently nearer to right edge than w2
            leftsum=leftsum-data[i-w1]+data[i]
            rightsum=rightsum-data[i]
            dif[i]=abs(leftsum/n1-rightsum/n2)*(n2/w2)^.5
        } else if (i<=w1 & (n-i)<w2) { #if currently nearer to left edge than w1 AND nearer to right edge than w2
            leftsum=leftsum+data[i]
            rightsum=rightsum-data[i]
            dif[i]=abs(leftsum/n1-rightsum/n2)*(n1/w1)^.5*(n2/w2)^.5
        } else { # if left,right edge farther than w1,w2
            leftsum=leftsum-data[i-w1]+data[i]
            rightsum=rightsum-data[i]+data[i+w2]
            dif[i]=abs(leftsum/n1-rightsum/n2)
        }  
        if(is.na(dif[i])) browser()
    }
    return(dif)
}

getPeaks <- function(t, removeZero=F) { # which points in a vector are peaks
    if (removeZero) t[t==0]=NA
    t=c(F,diff(sign(diff(t)))<0,F)
    t[is.na(t)]=F
    return(t)
}

smooth <- function(t,w=5) {
    s=t
    n=length(t)
    for (i in 1:n) {
        left=max(1,i-w+1):i
        right=(i+1):(min(n,i+w))
        s[i]=mean(t[c(left,i,right)])
    }
    return(s)
}


## This OLD function performs segmentation of a SINGLE sample.
segment.sample.old <- function(name,probes.txt,snps.txt,refseq.genelist,chromData) {
    separate=1e6
    chroms <- as.character(unique(probes.txt$Chromosome)); chroms=chroms[!is.na(chroms)]; chroms=chroms[order(chrom_n(chroms))]
    segments.txt <- NULL
    for (c in chroms) {
        #cat(c,' ')
        ix=probes.txt$Chromosome==c; ix[is.na(ix)]=F
        logr=probes.txt$Value[ix]
        lpos=(probes.txt$Start[ix]+probes.txt$End[ix])/2
        ix=snps.txt$Chromosome==c & !is.na(snps.txt$Value); ix[is.na(ix)]=F
        baf=snps.txt$Value[ix]
        bpos=(snps.txt$Start[ix]+snps.txt$End[ix])/2
        
        # centromere cutoff=1Mb
        dists=diff(lpos)
        cuts=c(0,which(dists>separate),length(lpos))
        for (i in 2:length(cuts)) {
            ix=(cuts[i-1]+1):cuts[i]
            bix=bpos>=lpos[ix][1] & bpos<=lpos[ix][length(ix)]
            segments.txt <- rbind(segments.txt,cbind(c,getSegments(logr[ix], lpos[ix], baf[bix], bpos[bix])))
        }
    }
    names(segments.txt)[1]='Chromosome'
    
    #browser()
    
    
    ## Compute data for segments -> segments.txt
    cat('Segment analysis:',name,'\n')
    segments.txt$Length.Mbp=round((segments.txt$End-segments.txt$Start)/1e6,2)
    segments.txt$Value=NA
    #segments.txt$Value.95CI=NA
    #segments.txt$Value.lowerCI=NA
    #segments.txt$Value.upperCI=NA
    #segments.txt$Value.sd=NA
    segments.txt$Markers=NA
    segments.txt$SNPs=NA
    segments.txt$Allelic.Imbalance=NA
    #segments.txt$Lesser.Allele.Ratio=NA
    segments.txt$Start.band=NA
    segments.txt$End.band=NA
    #segments.txt$Refseq.genes=addGenes(segments.txt[,1:3],refseq.genelist)$genes #Fixa genlistan
    
    probe.indices=segMatch(segments.txt,probes.txt)
    snp.indices=segMatch(segments.txt,snps.txt)
    
    for (s in 1:nrow(segments.txt)) {
        #cat(s)
        if (!is.na(probe.indices[s,1])) {
            logr=probes.txt$Value[probe.indices[s,1]:probe.indices[s,2]]
            #t=t.test(logr,conf.int=T,)
            segments.txt$Value[s]=round(median(logr,na.rm=T),3)
            #segments.txt$Value.mean[s]=round(t$estimate,3)
            #segments.txt$Value.95CI[s]=round(t$conf.int[2]-t$estimate,3)
            #segments.txt$Value.lowerCI[s]=round(t$conf.int[1]+median(logr,na.rm=T)-t$estimate,3)
            #segments.txt$Value.upperCI[s]=round(t$conf.int[2]+median(logr,na.rm=T)-t$estimate,3)
            #segments.txt$Value.sd[s]=round(sd(logr,na.rm=T),3)
            segments.txt$Markers[s]=length(logr)
        }
        
        if (!is.na(snp.indices[s,1])) {
            baf=snps.txt$Value[snp.indices[s,1]:snp.indices[s,2]]
            segments.txt$SNPs[s]=length(baf)
            baf=baf[!is.na(baf)]
            segments.txt$Allelic.Imbalance[s]=round(getAI(baf)[1],3)
            #segments.txt$Lesser.Allele.Ratio[s]=round(getLAF(baf),2)
        }
        #browser()
        c=as.character(segments.txt$Chromosome[s])
        segments.txt$Start.band[s]=paste(substr(c,4,6),as.character(chromData$name[chromData$chr==c &
                                                                                       chromData$chromStart<=segments.txt$Start[s] &
                                                                                       chromData$chromEnd>=segments.txt$Start[s]]),sep='')
        segments.txt$End.band[s]=paste(substr(c,4,6),as.character(chromData$name[chromData$chr==c &
                                                                                     chromData$chromStart<=segments.txt$End[s] &
                                                                                     chromData$chromEnd>=segments.txt$End[s]]),sep='')
    }
    return(segments.txt)
}
getBreaks_old <- function(data,w1,w2,difcut,plot=F) {
    #browser()
    if (length(data)<w1+w2) return (NULL)
    na=(is.na(data)) # na sites removed temporarily
    data=(data[!na]); data[data<-3]=-3
    n=length(data)
    dif=rep(0,n-1) # length of dif is all possible breakpoints including those too close to edge
    i=w1 # i is the current possible cutting site to be evaluated
    leftsum=sum(data[1:i])
    rightsum=sum(data[(i+1):(i+w2)])
    dif[w1]=abs(leftsum/w1-rightsum/w2)
    for (i in (w1+1):(n-w2)) { # all possible breakpoints given w1 and w2 
        leftsum=leftsum-data[i-w1]+data[i]
        rightsum=rightsum-data[i]+data[i+w2]
        dif[i]=abs(leftsum/w1-rightsum/w2)
    }
    if (plot) plot(dif)
    peaks=getPeaks(dif,F) & dif>difcut
    # Keep only best peak in each window:
    for (i in which(peaks)[order(dif[peaks],decreasing=T)]) {
        left=max(1,i-w1)
        right=min(length(dif),i+w2)
        if (max(dif[left:right])!=dif[i]) peaks[i]=F
    }
    if (plot) points(x=(1:length(peaks))[peaks],
                     y=dif[peaks],
                     pch=2,col='red')
    out=rep(F,length(na)) # vector of length of orig. data
    out[!na][which(peaks)]=T
    return(which(out))
}

getSegments_old <- function(logr, lpos, baf, bpos) {
    #browser()
    
    contig.start=round(min(min(lpos),min(c(bpos,Inf)))-12.5)
    contig.end=round(max(max(lpos),max(c(bpos,-Inf)))+12.5)
    
    breakpos1  <- breakpos2 <- NULL
    
    lpos=lpos[!is.na(logr)]
    logr=logr[!is.na(logr)]
    
    breakpos1=c(0,length(logr))
    
    # first split into longer segments, then find shorter ones in remaining stretches of data
    mainwindows=c(2000, 500, 70, 20, 10) # testing a set of window lenghs
    oppositewindows=c(2000, 500, 70, 20, 10)
    cutdifs=c(0.04, 0.08, 0.20, 0.4,0.7)
    
    
    for (i in 2:length(mainwindows)) { # For each set of main and opposite windows
        #cat('\n',i,'\n')
        for (j in 2:length(breakpos1)) if (breakpos1[j]-breakpos1[j-1] > mainwindows[i]+oppositewindows[i]) { # And for each long-enough already separated subsegment
            #cat(j)
            ix=(breakpos1[j-1]+1):breakpos1[j] # the current logr indices
            breaks1=getBreaks(logr[ix],mainwindows[i],oppositewindows[i],difcut=cutdifs[i],plot=F) # get potential breaks using mainwindow on left
            #breaks2=getBreaks(logr[ix],oppositewindows[i],mainwindows[i],difcut=cutdifs[i],plot=F) # get potential breaks using mainwindow on right
            breaks=breaks1#sort(unique(c(breaks1,breaks2))) # fuse breaks into a set of candidates
            
            ## Test if breaks are good ("undo")
            if (length(breaks)>0) {
                p <- d <- NULL; 
                for (k in length(breaks):1) {
                    if (k==1) left=0 else left=breaks[k-1]
                    if (k==length(breaks)) right=length(logr) else right=breaks[k+1]
                    p[k]=t.test(logr[(left+1):breaks[k]],logr[(breaks[k]+1):(right)])$p.value[[1]]
                    #d[k]=abs(mean(logr[(left+1):breaks[k]])-mean(logr[(breaks[k]+1):(right)]))
                    if (p[k]>0.0001) breaks=breaks[-k]
                } 
                
            }
            breakpos1=c(breakpos1,breaks+breakpos1[j-1]) # add these breakpoints into the complete set of cutting points, adjusting to relative position
        }
        breakpos1=sort(unique(breakpos1))
    }
    
    ## Test again if cuts are good ("undo")
    dcut=0.04
    for (pcut in c(1e-2,1e-3,1e-4,1e-5,1e-6)) if (length(breakpos1)>2) {
        p <- d <- rep(0,length(breakpos1));
        if (length(breakpos1)>2) for (i in (length(breakpos1)-1):2) {
            left=breakpos1[i-1]
            right=breakpos1[i+1]
            p[i]=t.test(logr[(left+1):breakpos1[i]],logr[(breakpos1[i]+1):(right)])$p.value[[1]]
            d[i]=abs(mean(logr[(left+1):breakpos1[i]])-mean(logr[(breakpos1[i]+1):(right)]))
            if (p[i]>pcut | d[i]<dcut) breakpos1=breakpos1[-i]
        } 
    }
    #browser()
    # i=6; plot(logr[breakpos1[i]:breakpos1[i+1]],pch=16,cex=0.2,col='#00000015')
    #plot(logr,pch=16,cex=0.2,col='#00000015')
    #segments(x0=breakpos1,x1=breakpos1,y0=-2,y1=2,col='#00FF00')
    
    ### convert breakpoints to positions
    logbreaks=(lpos[trim(breakpos1)]+lpos[trim(breakpos1)+1])/2
    
    
    ## get breakpoints from BAF using long windows
    baf=abs(baf-.5); baf[baf>0.4]=NA ## may need to be adjusted
    bpos=bpos[!is.na(baf)]
    baf=baf[!is.na(baf)]
    
    if (length(baf)>300) {
        breakpos2=c(0,length(baf))
        
        # first split into longer segments, then find shorter ones in remaining stretches of data
        mainwindows=c(100) # testing a set of window lenghs
        oppositewindows=c(100)
        cutdifs=c(0.05)
        
        for (i in 1:length(mainwindows)) { # For each set of main and opposite windows
            #cat('\n',i,'\n')
            for (j in 2:length(breakpos2)) if (breakpos2[j]-breakpos2[j-1] > mainwindows[i]+oppositewindows[i]) { # And for each long-enough already separated subsegment
                #cat(j)
                ix=(breakpos2[j-1]+1):breakpos2[j] # the current baf indices
                breaks1=getBreaks(baf[ix],mainwindows[i],oppositewindows[i],difcut=cutdifs[i],plot=F) # get potential breaks using mainwindow on left
                #breaks2=getBreaks(baf[ix],oppositewindows[i],mainwindows[i],difcut=cutdifs[i],plot=F) # get potential breaks using mainwindow on right
                breaks=breaks1#sort(unique(c(breaks1,breaks2))) # fuse breaks into a set of candidates
                
                ## Test if breaks are good ("undo")
                if (length(breaks)>0) {
                    p <- d <- NULL; 
                    for (k in length(breaks):1) {
                        if (k==1) left=0 else left=breaks[k-1]
                        if (k==length(breaks)) right=length(baf) else right=breaks[k+1]
                        p[k]=t.test(baf[(left+1):breaks[k]],baf[(breaks[k]+1):(right)])$p.value[[1]]
                        #d[k]=abs(mean(baf[(left+1):breaks[k]])-mean(baf[(breaks[k]+1):(right)]))
                        if (p[k]>0.00001) breaks=breaks[-k]
                    } 
                }
                breakpos2=c(breakpos2,breaks+breakpos2[j-1]) # add these breakpoints into the complete set of cutting points, adjusting to relative position
            }
            breakpos2=sort(unique(breakpos2))
        }
        
        t=breakpos2
        ## Test again if cuts are good ("undo")
        for (pcut in c(1e-6)) if (length(breakpos2)>2) {
            p <- d <- rep(0,length(breakpos2));
            for (i in (length(breakpos2)-1):2) {
                left=breakpos2[i-1]
                right=breakpos2[i+1]
                p[i]=t.test(baf[(left+1):breakpos2[i]],baf[(breakpos2[i]+1):(right)])$p.value[[1]]
                #d[i]=abs(mean(baf[(left+1):breakpos2[i]])-mean(baf[(breakpos2[i]+1):(right)]))
                if (p[i]>pcut) breakpos2=breakpos2[-i]
            } 
        }
        #browser()
        #plot(baf,pch=16,cex=0.2,col='#00000015')
        #segments(x0=breakpos2,x1=breakpos2,y0=-2,y1=2,col='#FF0000')
        
        
        ### convert breakpoints to positions
        bafbreaks=(bpos[trim(breakpos2)]+bpos[trim(breakpos2)+1])/2
        
        # prune BAF breakpoints, no nearer than 300k to CN breaks
        if (length(bafbreaks)>0 & length(logbreaks)>0) for (i in length(bafbreaks):1) {
            d=min(abs(bafbreaks[i]-logbreaks))
            if (d<.3e5) bafbreaks = bafbreaks[-i]
        }
    } else bafbreaks=numeric(0)
    
    breakpos=c(contig.start,round(sort(c(logbreaks,bafbreaks))),contig.end)
    
    ## return segments
    Start=trim(breakpos,0,1)
    End=trim(breakpos,1,0)
    return(data.frame(Start,End))
}
